# Memory-interceptor

Memory-interceptor is a tool that intercepts calls to memory allocation functions (eg. malloc, realloc, free, etc.), allowing to build memory analysis tools such as NumaMMa (https://numamma.github.io/numamma/)